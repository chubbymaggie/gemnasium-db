# Gemnasium DB

This repository contains all the security advisories of the Gemnasium database.
It's not the Gemnasium DB itself but can be considered as a single source of truth:
this is where new advisories are submitted so it's always in sync with the Gemnasium DB.

This project is used both to search for existing advisories and submit new ones.

The GitLab team constantly improves this vulnerability database
by checking external sources on a regular basis, and contributing their findings to this repo.
Learn more on the [external sources](SOURCES.md) and how the GitLab team tracks them.

## Licensing

This vulnerability database is available freely under the [GitLab Security Alert Database Terms](./TERMS.md), please review them before any usage.

## Contributing

If you know about a vulnerability that isn't listed in this repo,
you can contribute to the Gemnasium database by opening an issue,
or even submit the vulnerability as a YAML file in a merge request.
Please review the [contribution guidelines](CONTRIBUTING.md).

## Publishing

The maintainers of this project use the
[gemnasium-db-toolbox](https://gitlab.com/gitlab-org/security-products/gemnasium-db-toolbox)
command line tool to publish the vulnerabilities to the Gemnasium server.

## Directory structure

Each YAML file of the repo corresponds to a single security advisory.
The file path is made of the slug of the affected package (type and name),
and the identifier of the advisory, following this pattern:

```
package_slug/advisory_identifier.yml
```

For instance, security advisory [CVE-2019-11324](https://nvd.nist.gov/vuln/detail/CVE-2019-11324)
related to Python package [urllib3](https://pypi.org/project/urllib3/) is:

```
pypi/urllib3/CVE-2019-11324.yaml
```

## Package slug

The package [slug](https://en.wikipedia.org/wiki/Clean_URL#Slug) is made of
the package type and the fully qualified package name separated by a slash `/`.

The supported package types are:

```
gem
maven
npm
packagist
pypi
```

These correspond to:

- gems from [rubygems.org](http://rubygems.org)
- Java Maven packages from [Maven Central](https://repo1.maven.org/maven2/)
- npm packages from [npmjs.com](https://www.npmjs.com/)
- PHP Composer packages from [packagist.org](https://packagist.org/)
- Python packages from [pypi.org](https://pypi.org/)

For npm packages, the package name may include a [npm scope](https://docs.npmjs.com/misc/scope).
For instance, the package slug of [@babel/cli](https://www.npmjs.com/package/@babel/cli) is:

```
npm/@babel/cli
```

For Maven packages, the package name is made of the `groupId` and the `artifactId` separted by a slash `/`.
For instance, the package slug of [jackson-databind](https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/) is:

```
maven/com.fasterxml.jackson.core/jackson-databind
```

## YAML schema

* `identifier` (string): The CVE id (preferred) or any public identifier.
* `package_slug` (string): Package type and package name separated by a slash.
* `title` (string): A short description of the security flaw.
* `description` (string): A long description of the security flaw and the possible risks.
   The [CommonMark](https://spec.commonmark.org/0.28/) flavor of Markdown can be used.
* `date` (string): The date on which the advisory was made public, in ISO-8601 format.
* `affected_range` (string): The range of affected versions. Machine-readable syntax used by the package manager.
* `affected_versions` (string): The range of affected versions. Human-readable version for display.
* `fixed_versions` (array of strings): The versions fixing the vulnerability. The order is not relevant.
* `not_impacted` (string, optional): Environments not affected by the vulnerability.
* `solution` (string, optional): How to remediate the vulnerability.
* `credit` (string, optional): The names of the people who reported the vulnerability or helped fixing it.
* `urls` (array of strings): URLs of: detailed advisory, documented exploit, vulnerable source code, etc.
   The order is not relevant.
* `uuid` (string): Unique (RFC-4122 compliant) identifier in Gemnasium DB. Set when publishing the vulnerability.

`affected_range` is processed by Gemnasium when publishing the security advisory
whereas `affected_versions` is simply a string presented to users.

The syntax to be used in `affected_range` depends on the package type:
- `gem`: [gem requirement](https://guides.rubygems.org/specification-reference/#add_runtime_dependency)
- `maven`: [Maven Dependency Version Requirement Specification](https://maven.apache.org/pom.html#Dependency_Version_Requirement_Specification)
- `npm`: [node-semver](https://github.com/npm/node-semver#ranges)
- `php`: [PHP Composer version constraints](https://getcomposer.org/doc/articles/versions.md#writing-version-constraints)
- `pypi`: [PEP440](https://www.python.org/dev/peps/pep-0440/#version-specifiers)
  with the addition of the OR `||` operator

Here's a sample document:

```
---
identifier: CVE-2019-11324
package_slug: pypi/urllib3
title: Weak Authentication Caused By Improper Certificate Validation
description: The library mishandles certain cases where the desired set of CA certificates
  is different from the OS store of CA certificates, which results in SSL connections
  succeeding in situations where a verification failure is the correct outcome. This
  is related to use of the ssl_context, ca_certs, or ca_certs_dir argument.
date: 2019-04-18
affected_range: "<=1.24.1"
fixed_versions:
- 1.24.2
affected_versions: Prior to 1.24.2
not_impacted: 
solution: Upgrade to the latest version
credit: Christian Heimes
urls:
- https://nvd.nist.gov/vuln/detail/CVE-2019-11324
- http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2019-11324
- https://www.openwall.com/lists/oss-security/2019/04/19/1
uuid: 37bd3208-7f9f-4d23-86bd-a422d455fd45
```
